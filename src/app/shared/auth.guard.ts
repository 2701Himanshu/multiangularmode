import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  
  	constructor(public router:Router){}

  	canActivate() {
	    // Imaginary method that is supposed to validate an auth token
	    // and return a boolean
	    return localStorage.getItem('user_details') ? true : this.goToLogin();
	}

	goToLogin(): boolean {
	    this.router.navigateByUrl('/login');
	    return false;
	}
}