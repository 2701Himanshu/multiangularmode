import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { DashboardRoutingModule } from './dashboard.routing';
import { DashboardPageComponent } from './components/dashboard-page/dashboard-page.component';
import { DashboardComponent } from './dashboard.component';

import { FlashMessagesModule } from 'angular2-flash-messages';

import { TestPipe } from '../../shared/test.pipe';

import {
  MatListModule
} from '@angular/material';

import { 
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatSidenavModule,
    MatGridListModule
} from '@angular/material';

import { NavigationComponent } from './components/navigation/navigation.component';
import { Comp1Component } from './components/comp1/comp1.component';
import { Comp2Component } from './components/comp2/comp2.component';
import { Comp3Component } from './components/comp3/comp3.component';
import { Comp4Component } from './components/comp4/comp4.component';

@NgModule({
  declarations: [
  	DashboardPageComponent,
    DashboardComponent,
    NavigationComponent,
    TestPipe,
    Comp1Component,
    Comp2Component,
    Comp3Component,
    Comp4Component
  ],
  imports: [
    FormsModule,
    CommonModule,
    DashboardRoutingModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatButtonModule,
    MatListModule,
    MatGridListModule,
    FlashMessagesModule.forRoot()
  ],
  exports: [
  	DashboardRoutingModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatButtonModule,
    MatListModule,
    MatGridListModule
  ],
  providers: [],
  bootstrap: [DashboardPageComponent]
})
export class DashboardModule {
  constructor(){
    console.log('dashboard module loaded');
  }
}
