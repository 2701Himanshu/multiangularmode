import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DataService } from "../../../../shared/data.service";
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-comp2',
  templateUrl: './comp2.component.html',
  styleUrls: ['./comp2.component.scss']
})
export class Comp2Component implements OnInit {

  @Output() change = new EventEmitter();

  constructor(private dataService: DataService, private flash:FlashMessagesService) { }
  msg:string;
  message:string;
  
  ngOnInit() {
    this.dataService.currentMessage.subscribe(data=> this.message = data);
  }

  sendData(){
  	this.change.emit({data: this.msg});
  }

  showSuccessMsg(){
    this.flash.show('Success Message !', {
      cssClass: 'alert-success'
    });
  }

  showFailureMsg(){
    this.flash.show('Failure Message !', {
      cssClass: 'alert-failure'
    });
  }
}
