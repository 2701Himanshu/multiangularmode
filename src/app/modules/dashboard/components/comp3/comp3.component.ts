import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-comp3',
  templateUrl: './comp3.component.html',
  styleUrls: ['./comp3.component.scss']
})
export class Comp3Component implements OnInit {

  @Input() num: number;
  numb: number;
  pattern1_code:string;
  pattern2_code:string;
  constructor() { }

  ngOnInit() {
  	this.numb = this.num;
 //  	let pattern2 = document.querySelector('#pattern2');
 //  	pattern2.innerHTML = "";
 //  	let rows=5;
	// for(let i=rows; i>=1; i--)
	// {
	// 	for(let j=1; j<=i; j++)
	//  	{
	//    		pattern2.innerHTML += '* ';
	//   	}
	// 	pattern2.innerHTML += '<br/> ';
	// }

	// this.pattern2_code = `
	// let pattern1 = document.querySelector('#pattern2');
 //  	pattern2.innerHTML = "";
 //  	let rows=5;
	// for(let i=rows; i>=1; i--)
	// {
	// 	for(let j=1; j<=i; j++)
	//  	{
	//    		pattern2.innerHTML += '* ';
	//   	}
	// 	pattern2.innerHTML += '<br/> ';
	// }`;
	/*
		* * * * *
		* * * *
		* * *
		* * 
		*
	*/
	let pattern1 = document.querySelector('#pattern1');
  	pattern1.innerHTML = "";
  	let row= this.num;

  	for(let i=row; i>=0; i--){
  		for(let s=0; s<i; s++){
  			pattern1.innerHTML += "&nbsp;&nbsp;";
  		}
  		for(let j=i; j<row; j++){
  			pattern1.innerHTML += "&nbsp;*&nbsp;";
  		}
  		pattern1.innerHTML += "<br/>";
  	}

  	this.pattern1_code = `
  	let pattern1 = document.querySelector('#pattern1');
  	pattern1.innerHTML = "";
  	let row= this.num;

  	for(let i=row; i>=0; i--){
  		for(let s=0; s<i; s++){
  			pattern1.innerHTML += "&nbsp;&nbsp;";
  		}
  		for(let j=i; j<row; j++){
  			pattern1.innerHTML += "&nbsp;*&nbsp;";
  		}
  		pattern1.innerHTML += "<br/>";
  	}`;

  	/*
		    *
		   * *
		  * * * 
	     * * * *
	 	* * * * *
	*/
  }

}
