import { Component, OnInit, Input } from '@angular/core';
import { DataService } from '../../../../shared/data.service';

@Component({
  selector: 'app-comp1',
  templateUrl: './comp1.component.html',
  styleUrls: ['./comp1.component.scss']
})
export class Comp1Component implements OnInit {

  @Input() message : string;
  
  constructor( private data: DataService ) { }

  ngOnInit() {
  }

  setItem(val){
  	console.log(val);
  	this.data.changeMessage(val);
  }
}
