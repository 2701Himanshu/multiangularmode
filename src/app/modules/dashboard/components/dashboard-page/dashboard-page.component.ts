import { Component, OnInit, ViewChild } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { Comp2Component } from '../comp2/comp2.component';
import { Comp3Component } from '../comp3/comp3.component';

@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.scss']
})
export class DashboardPageComponent implements OnInit {

  val:number = 10;

  str:string;
  msg:string;

  evtMsg:string;

  @ViewChild(Comp2Component) comp2;
  @ViewChild(Comp3Component) comp3;
  
  numb: number;
  constructor() { 
  	console.log('dashboard page component loaded.');
  }

  ngOnInit() {
      this.str = 'somE stRinG';
      this.msg = this.comp2.msg;

      this.numb = this.comp3.numb;
  }

  getMessage($event){
    this.evtMsg = $event.target.value;
  }
}
