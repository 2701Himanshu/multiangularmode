import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-comp4',
  templateUrl: './comp4.component.html',
  styleUrls: ['./comp4.component.scss']
})
export class Comp4Component implements OnInit {

  constructor() { }
  taskList: any = [];
  task_name:string;
  ngOnInit() {
  	this.getTaskList();
  }

  addTask(){
  	this.taskList.push({name : this.task_name});
  	this.task_name = '';
  	this.updateDB();
  }

  getTaskList(){
  	if(window.localStorage.getItem('TaskList') == null){
  		this.taskList = [];	
  	} else {
  		this.taskList = JSON.parse(window.localStorage.getItem('TaskList'));
  	}
  }

  deleteTask(task){
  	let index = this.taskList.indexOf(task);
  	this.taskList.splice(index, 1);
  	this.updateDB();
  }

  updateDB(){
  	window.localStorage.removeItem('TaskList');
  	window.localStorage.setItem('TaskList', JSON.stringify(this.taskList));
  }
}
