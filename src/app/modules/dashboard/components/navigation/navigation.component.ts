import { Component, OnInit, ElementRef } from '@angular/core';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  constructor(private hostElement: ElementRef) {
  }

  ngOnInit() {
  	(<HTMLElement>document.querySelector('.example-container')).style.height = (window.screen.height-140)+"px";
  	// console.log(this.hostElement.nativeElement.outerHTML);
  };

}
