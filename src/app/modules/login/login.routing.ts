import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// import components for routing
import { LoginComponent } from './login.component';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { SignupComponent } from './components/signup/signup.component';
import { ForgetPasswordComponent } from './components/forget-password/forget-password.component';

import { AuthGuard } from '../../shared/auth.guard';

const routes: Routes = [
	{
		path: '', component: LoginComponent,
	    children: [
	        { path: 'login', component: LoginPageComponent },
	        { path: 'register', component: SignupComponent },
	        { path: 'forgotpassword', component: ForgetPasswordComponent },
	        { path: 'dash', loadChildren: '../dashboard/dashboard.module#DashboardModule', canActivate: [AuthGuard]},
	        { path: '', redirectTo: 'login' }
	    ]
	}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }
