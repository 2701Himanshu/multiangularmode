import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { LoginRoutingModule } from './login.routing';

import { LoginComponent } from './login.component';
import { SignupComponent } from './components/signup/signup.component';
import { ForgetPasswordComponent } from './components/forget-password/forget-password.component';
import { LoginPageComponent } from './components/login-page/login-page.component';

import { AuthGuard } from '../../shared/auth.guard';
// material module
import { 
    MatCardModule, 
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatGridListModule
} from '@angular/material';

@NgModule({
  declarations: [
  	LoginComponent, 
  	SignupComponent, 
  	ForgetPasswordComponent, 
  	LoginPageComponent
  ],
  imports: [
    MatCardModule, 
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule, 
    MatInputModule,
    MatButtonModule,
    MatGridListModule,
    LoginRoutingModule],
  exports: [
    MatCardModule, 
    MatInputModule,
    FormsModule,
    MatButtonModule,
    MatGridListModule,
    MatFormFieldModule],
  providers: [AuthGuard],
  bootstrap: [LoginComponent]
})
export class LoginModule { 
	constructor(){
		console.log('Login Module loaded');
	}
}
