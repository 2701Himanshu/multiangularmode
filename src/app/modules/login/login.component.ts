import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

@Component({
	selector: 'app-login',
	template: `<router-outlet></router-outlet>`,
	styleUrls: ['./login.component.scss'],
	encapsulation: ViewEncapsulation.None
})

export class LoginComponent{
	title = 'Login Components';
	constructor(public router: Router){
		console.log('Login Component loaded');
	}

	ngAfterContentInit(){
		let loginStatus = window.localStorage.getItem('user_details') == undefined ? '' : this.goToDashboard();
	}

	goToDashboard(){
		this.router.navigateByUrl('/app/dash/dashboard');
	}
}