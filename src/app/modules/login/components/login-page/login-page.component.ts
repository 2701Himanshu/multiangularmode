import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

  username:string;
  password:string;
  constructor(private router: Router) { }

  ngOnInit() {
  }

  login(){
    if(this.username == "" || this.password == ""){
      alert('Please provide username and password.');
      return;
    }

    let dataToSave = {
      user: this.username,
      pass: this.password
    };
  	alert('Login Successfully!');
    window.localStorage.setItem('user_details', JSON.stringify(dataToSave));
    this.router.navigateByUrl('/app/dash');
  }
}
