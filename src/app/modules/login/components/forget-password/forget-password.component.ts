import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.scss']
})
export class ForgetPasswordComponent implements OnInit {

  email:string;
  constructor(private router: Router) { }

  ngOnInit() {
  }

  submit(){
  	console.log(this.email);
  	alert('Your password has beed changed, Please check your email.');
  	this.router.navigateByUrl("/app/login");
  }
}
