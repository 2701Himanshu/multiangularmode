import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginModule } from './modules/login/login.module';


const routes: Routes = [
    { path: 'app', loadChildren: './modules/login/login.module#LoginModule' },
	{ path: '', pathMatch: 'full', redirectTo:'app' },
	{ path: '**', redirectTo: 'app' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
